// Programa: Tipo Aluno
// Descrição: exemplo de uso de estrutura para representar os dados de
//		      um aluno.
// Autor: Alyppyo Coutinho
// Data: 02/05/2019

// Cabeçalhos
#include <stdio.h>

// Estruturas & Enumerações
enum SituacaoAluno {
	APR, APRN, REPO, REP
};

struct aluno {
	float nota1, nota2, nota3, media;
	enum SituacaoAluno situacao;
};

// Função principal
int main() {
	// Declaração de variáveis
	struct aluno novoAluno;

	// Apresentação do programa
	printf("-- Tipo Aluno --\n");

	// Solicitação de dados
	printf("- Entre com a primeira nota: ");
	scanf("%f", &novoAluno.nota1);

	printf("- Entre com a segunda nota: ");
	scanf("%f", &novoAluno.nota2);

	printf("- Entre com a terceira nota: ");
	scanf("%f", &novoAluno.nota3);

	// Impressão dos dados recebidos
	printf("\n-- Dados do Aluno --\n");
	printf("> Nota 1: %f\n", novoAluno.nota1);
	printf("> Nota 2: %f\n", novoAluno.nota2);
	printf("> Nota 3: %f\n", novoAluno.nota3);	

	// Retorno da função
	return 0;
}




