// Programa: Hora do duelo
// Descrição: descreve o protótipo básico para um jogo de duelo de cartas.
// Autor: Alyppyo Coutinho
// Data: 06/06/2019

// Cabeçalhos
#include <stdio.h>
#include <stdlib.h>

// Struct
typedef struct {
	int * ataque;
	int * defesa;
} Baralho;

// Função principal
int main() {
	// Declaração de variáveis
	int tam, i;
	Baralho jogador1, jogador2;

	// Apresentação do programa
	puts("-- Hora do Duelo --");

	// Solicitação da quantidade de cartas
	printf("- Entre com a quantidade de cartas dos baralhos: ");
	scanf("%d", &tam);

	// Alocação dinâmica dos baralhos
	jogador1.ataque = (int *) malloc(tam * sizeof(int));

	if(jogador1.ataque == NULL) {
		puts("> Erro ao alocar vetor! Abortando...");
		return EXIT_FAILURE;
	}

	jogador1.defesa = (int *) malloc(tam * sizeof(int));

	if(jogador1.defesa == NULL) {
		puts("> Erro ao alocar vetor! Abortando...");
		return EXIT_FAILURE;
	}

	jogador2.ataque = (int *) malloc(tam * sizeof(int));

	if(jogador2.ataque == NULL) {
		puts("> Erro ao alocar vetor! Abortando...");
		return EXIT_FAILURE;
	}

	jogador2.defesa = (int *) malloc(tam * sizeof(int));

	if(jogador2.defesa == NULL) {
		puts("> Erro ao alocar vetor! Abortando...");
		return EXIT_FAILURE;
	}

	// Solicitação de valores das cartas do baralho
	for(i = 0; i < tam; i++) {
		do {
			// Solicitar pontos de ataque
			printf("- Entre com os pontos de ataque da %dª carta do jogador 1: ", i+1);
			scanf("%d", &jogador1.ataque[i]);

			// Mensagem de erro ao entrar com valor inválido
			if(jogador1.ataque[i] < 0 || jogador1.ataque[i] > 99)
				puts("> Valor inválido! Entre com um número entre 0 e 99!\n");
		} while(jogador1.ataque[i] < 0 || jogador1.ataque[i] > 99);

		// Solicitar pontos de defesa
		do {
			printf("- Entre com os pontos de defesa da %dª carta do jogador 1: ", i+1);
			scanf("%d", &jogador1.defesa[i]);

			if(jogador1.defesa[i] < 0 || jogador1.defesa[i] > 99)
				puts("> Valor inválido! Entre com um número entre 0 e 99!\n");
		} while(jogador1.defesa[i] < 0 || jogador1.defesa[i] > 99);
	}

	// Separar obtenção de cartas
	puts("");

	for(i = 0; i < tam; i++) {
		do {
			// Solicitar pontos de ataque
			printf("- Entre com os pontos de ataque da %dª carta do jogador 2: ", i+1);
			scanf("%d", &jogador2.ataque[i]);

			// Mensagem de erro ao entrar com valor inválido
			if(jogador2.ataque[i] < 0 || jogador2.ataque[i] > 99)
				puts("> Valor inválido! Entre com um número entre 0 e 99!\n");
		} while(jogador2.ataque[i] < 0 || jogador2.ataque[i] > 99);

		// Solicitar pontos de defesa
		do {
			printf("- Entre com os pontos de defesa da %dª carta do jogador 2: ", i+1);
			scanf("%d", &jogador2.defesa[i]);

			if(jogador2.defesa[i] < 0 || jogador2.defesa[i] > 99)
				puts("> Valor inválido! Entre com um número entre 0 e 99!\n");
		} while(jogador2.defesa[i] < 0 || jogador2.defesa[i] > 99);
	}

	// Liberar memória
	free(jogador1.ataque);
	free(jogador1.defesa);
	free(jogador2.ataque);
	free(jogador2.defesa);

	// Retorno da função
	return EXIT_SUCCESS;
}






