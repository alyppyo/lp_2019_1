// Programa: Transformação de caso
// Descrição: recebe o nome completo do usuário e o separa em seus componentes
//			  individuais, além de converter seus caracteres para maiúsculos.
// Autor: Alyppyo Coutinho
// Data: 16/05/2019

// Cabeçalho
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// Função principal
int main() {
	// Declaração de constantes
	const int TAM = 51;

	// Declaração de variáveis
	char texto[TAM];
	char * indice;
	int i, qntChars;

	// Apresentação do programa
	puts("-- Nome Completo --");

	// Solicitar o nome ao usuário
	printf("- Entre com o seu nome completo: ");
	//scanf("%s", texto);
	fgets(texto, TAM, stdin);

	// Procurar pela quebra de linha dentro do texto informado
	indice = strchr(texto, '\n');

	// Verificar se existe quebra de linha
	if(indice != NULL) *indice = '\0';

	// Percorrer o nome quebrando-o em seus componentes básicos
	qntChars = strlen(texto);
	for(i = 0; i < qntChars; i++) {
		if(texto[i] == ' ') texto[i] = '\n';
		else texto[i] = toupper(texto[i]);
	}

	// Divulgar o nome obtido
	printf("\n> Nome:\n%s\n", texto);

	// Retorno da função
	return 0;
}





