// Programa: Contador recursivo
// Descrição: recebe um valor do usuário e realiza uma contagem de 1 até ele, 
//		  	  sendo a contagem feita de forma recursiva.
// Autor: Alyppyo Coutinho
// Data: 02/05/2019

// Cabeçalhos
#include <stdio.h>

// Assinatura
void contadorRecursivo(int cont, int numero);

// Função principal
int main() {
	// Declaração de variáveis
	int numero;

	// Apresentação do programa
	printf("-- Contador Recursivo --\n");

	// Solicitar um número ao usuário
	printf("> Informe um valor inteiro maior que 1: ");
	scanf("%d", &numero);

	// Realizar contagem
	contadorRecursivo(1, numero);

	// Quebra de linha
	printf("\n");

	// Retorno da função
	return 0;
}


// Função: contadorRecursivo
// Descrição: recebe um número do usuário e imprime uma contagem de 1 até ele.
void contadorRecursivo(int cont, int numero) {
	// Imprimir o valor do contador
	printf("%d ", cont);


	if(cont < numero) contadorRecursivo(cont+1, numero);
}









