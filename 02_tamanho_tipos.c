#include <stdio.h>

int main() {
	printf("-- Tamanhos dos Principais Tipos --\n");
	printf("int: %ld byte(s)\n", sizeof(int));
	printf("float: %ld byte(s)\n", sizeof(float));
	printf("double: %ld byte(s)\n", sizeof(double));
	printf("char: %ld byte(s)\n", sizeof(char));
	
	return 0;
}