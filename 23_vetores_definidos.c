// Programa: Vetores definidos pelo usuário
// Descrição: exemplo de alocação de vetores de diversos tipos.
// Autor: Alyppyo Coutinho
// Data: 23/05/2019

// Cabeçalhos
#include <stdio.h>
#include <stdlib.h>

// Função principal
int main() {
	// Declaração de variáveis
	int tam;
	int * vet1 = NULL;
	double * vet2 = NULL;
	char * vet3 = NULL;

	// Apresentação do programa
	puts("-- Vetores Personalizados --");

	// Solicitar o tamanho dos vetores
	printf("- Entre com o tamanho do vetor: ");
	scanf("%d", &tam);

	// Alocação de memória para vetor de inteiros
	vet1 = (int *) malloc(tam * sizeof(int));

	// Verificar se alocação foi bem-sucedida
	if(vet1 == NULL) {
		puts("> Não foi possível alocar o vetor de inteiros!");
		return EXIT_FAILURE;
	}

	// Alocação de memória para vetor de double
	vet2 = (double *) malloc(tam * sizeof(double));

	// Verificar se alocação foi bem-sucedida
	if(vet2 == NULL) {
		puts("> Não foi possível alocar o vetor de doubles!");
		return EXIT_FAILURE;
	}

	// Alocação de memória para vetor de char's
	vet3 = (char *) malloc(tam * sizeof(char));

	// Verificar se alocação foi bem-sucedida
	if(vet3 == NULL) {
		puts("> Não foi possível alocar o vetor de caracteres!");
		return EXIT_FAILURE;
	}

	// Apresentar endereços de memória
	printf("\n> int: %p / double: %p / char: %p\n", vet1, vet2, vet3);

	// Liberar memória alocada
	free(vet1);
	free(vet2);
	free(vet3);

	// Retorno da função
	return EXIT_SUCCESS;
}





