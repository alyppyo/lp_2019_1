// Programa: Fatorial
// Descrição: recebe um valor inteiro não-negativo do usuário e
//			  calcula o seu fatorial.
// Autor: Alyppyo Coutinho
// Data: 26/03/2019

// Cabeçalho
#include <stdio.h>

// Assinaturas das funções
double fatorial(int numero);

// Função principal
int main() {
	// Declaração de variáveis
	int valor;

	// Apresentação do programa
	printf("-- Fatorial --\n");

	// Solicitar um valor inteiro não-negativo
	printf("- Entre com um valor inteiro não-negativo: ");
	scanf("%d", &valor);
	
	// Divulgar o fatorial
	printf("> Fatorial: %lf\n", fatorial(valor));

	// Retorno da função
	return 0;
}


// Função: fatorial
// Descrição: recebe um valor inteiro não-negativo e calcula
//			  o seu fatorial.
double fatorial(int numero) {
	// Declaração de variáveis
	double fat = 1;

	// Laço para calcular o fatorial
	while(numero >= 2) {
		fat *= numero--;
	}

	// Retornar o fatorial
	return fat;
}



