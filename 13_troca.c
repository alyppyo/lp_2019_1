// Programa: Troca
// Descrição: troca o valor de duas variáveis em uma função auxiliar.
// Autor: Alyppyo Coutinho
// Data: 23/04/2019

// Cabeçalho
#include <stdio.h>

// Função: troca
// Descrição: troca o valor de duas variáveis passadas como parâmetros.
void troca(int * a, int * b) {
	int aux = *a;
	*a = *b;
	*b = aux;
}

// Função principal
int main() {
	// Declaração de variáveis
	int x = 10, y = 20;

	// Trocar o valor das variáveis
	troca(&x, &y);

	// Imprimir o valor das variáveis
	printf("x = %d, y = %d\n", x, y);

	// Retorno da função
	return 0;
}
