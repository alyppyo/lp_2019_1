// Programa: Valor em intervalo
// Descrição: recebe dois valores que delimitam um intervalo e, em seguida, recebe um terceiro
//			  que será testado. Ao final, indica se o terceiro valor está dentro do intervalo
//			  definido pelos outros dois.
// Autor: Alyppyo Coutinho
// Data: 21/03/2019

// Cabeçalhos
#include <stdio.h>

// Função principal
int main() {
	// Declaração de variáveis
	int inicio, fim;  // Delimitam o intervalo 
	int valorTestado; // Valor a ser testado
	int aux; // Variável utilizada para troca

	// Apresentação do programa
	printf("-- Valor em Intervalo --\n");

	// Solicitar ao usuário os dois valores que delimitam o intervalo
	printf("- Entre com o valor de início do intervalo: ");
	scanf("%d", &inicio);

	printf("- Entre com o valor de fim do intervalo: ");
	scanf("%d", &fim);

	// Checar se o intervalo está adequado. Caso não esteja, efetua
	// uma troca entre os valores.
	if(inicio > fim) {
		printf("> Valores inadequados para o intervalo. Para sua comodidade, eles foram invertidos.\n");

		// Trocar valores do intervalo
		aux = inicio;
		inicio = fim;
		fim = aux;
	}

	// Solicitar o valor a ser testado
	printf("\n- Entre com o valor a ser testado: ");
	scanf("%d", &valorTestado);

	// Divulgar se o valor está dentro do intervalo definido
	printf("> %d %sestá entre %d e %d.\n", valorTestado,
										   (valorTestado < inicio || valorTestado > fim) ? "não " : "",
										   inicio,
										   fim);

	/* Alternativa 2
	printf("> %d ", valorTestado);
	if(valorTestado < inicio || valorTestado > fim) printf("não ");
	printf("está entre %d e %d.\n", inicio, fim);*/

	/* Alternativa 1
	if(valorTestado >= inicio && valorTestado <= fim)
		printf("> %d está entre %d e %d.\n", valorTestado, inicio, fim);
	else 
		printf("> %d não está entre %d e %d.\n", valorTestado, inicio, fim);*/

	// Retorno da função
	return 0;
}





