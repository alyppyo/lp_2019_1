#include <stdio.h>

int main() {
	int inteiro = 123;
	float realFlt = 321.234567891011121314151617;
	double realDbl = 321.234567891011121314151617;
	char caractere = 'A';
	char * string = "Texto qualquer";

	printf("Variável 'inteiro': %d\n", inteiro);
	printf("Variável 'realFlt': %.10f\n", realFlt);
	printf("Variável 'realDbl': %.10lf\n", realDbl);
	printf("Variável 'caractere': %c\n", caractere);
	printf("Variável 'string': %s\n", string);
	
	return 0;
}
