// Programa: Área do círculo
// Descrição: recebe do usuário o valor do raio e calcula a
//		      área do círculo correspondente.
// Autor: Alyppyo Coutinho
// Data: 14/03/2019

// Cabeçalho
#include <stdio.h>
#include <math.h>

// Função principal
int main() {
	// Declaração de variáveis
	float raio, area;

	// Apresentação do programa
	printf("-- Área do círculo --\n");
	
	// Solicitar números ao usuário
	printf("- Entre com o valor do raio: ");
	scanf("%f", &raio);

	// Calcular a área do círculo
	area = M_PI*pow(raio, 2);

	// Divulgar o valor da área
	printf("> Área: %f\n", area);

	// Retorno da função
	return 0;
}