// Programa: Combinação simples
// Descrição: dadas a quantidade de elementos no conjunto e a
//			  quantidade de elementos no subconjunto, calcula
//			  uma combinação simples.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019

// Cabeçalho
#include <stdio.h>

// Assinaturas das funções
int combinacaoSimples(int n, int p);
double fatorial(int numero);

// Função principal
int main() {
	// Declaração de variáveis
	int qntElemConj, qntElemSubconj;

	// Apresentação do programa
	printf("-- Combinação Simples --\n");

	// Solicitar os valores para a combinação simples
	printf("- Entre com o número de elementos no conjunto: ");
	scanf("%d", &qntElemConj);

	printf("- Entre com o número de elementos no subconjunto: ");
	scanf("%d", &qntElemSubconj);
	
	// Divulgar a quantidade de combinações
	printf("> Quantidade de combinações: %d\n", combinacaoSimples(qntElemConj, qntElemSubconj));

	// Retorno da função
	return 0;
}

// Função: combinacaoSimples
// Descrição: recebe a quantidade de elementos em um conjunto e a
//            a quantidade de elementos que se deseja nos subconjuntos
//			  e retorna a quantidade de combinações possíveis.
int combinacaoSimples(int n, int p) {
	// Retorno da função
	return fatorial(n) / (fatorial(p) * fatorial(n-p));
}

// Função: fatorial
// Descrição: recebe um valor inteiro não-negativo e calcula
//			  o seu fatorial.
double fatorial(int numero) {
	// Declaração de variáveis
	double fat = 1;

	// Laço para calcular o fatorial
	while(numero >= 2) {
		fat *= numero--;
	}

	// Retornar o fatorial
	return fat;
}



