// Programa: Tabuada
// Descrição: imprime a tabuada de 1 a 10 utilizando uma função de apoio.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019

// Cabeçalho
#include <stdio.h>

// Assinaturas
void tabuada(int numero);

// Função principal
int main() {
	// Declaração de variáveis
	int cont;

	// Apresentação do programa
	printf("-- Tabuadas de 1 a 10 --\n");

	// Divulgação da tabuada de 1 a 10
	for(cont = 1; cont <= 10; cont++) tabuada(cont);

	// Retorno da função
	return 0;
}

// Função: tabuada
// Descrição: dado um número passado como parâmetro, imprime a sua tabuada.
void tabuada(int numero) {
	// Declaração de variáveis
	int cont;

	// Laço para impressão da tabuada
	printf("\n-- Tabuada de %d --\n", numero);
	for(cont = 1; cont <= 10; cont++) {
		printf("  %d x %d = %d\n", numero, cont, numero*cont);
	}
}


