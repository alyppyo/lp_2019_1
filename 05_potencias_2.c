// Programa: Potências de 2
// Descrição: apresenta uma tabela contendo as potências de 2 até 2
//            elevado a 64.
// Autor: Alyppyo Coutinho
// Data: 19/03/2019

// Cabeçalhos
#include <stdio.h>
#include <math.h>

// Função principal
int main() {
    // Declaração de variáveis
    int expoente = 1;

    // Apresentação do programa
    printf("-- Potências de 2 --\n");

    // Impressão da tabela
    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));
    expoente *= 2;

    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));
    expoente *= 2;

    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));
    expoente *= 2;

    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));
    expoente *= 2;

    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));
    expoente *= 2;

    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));
    expoente *= 2;

    printf("> 2^%d: %.0f\n", expoente, pow(2, expoente));

    // Retorno da função
    return 0;
}

