// Programa: Coordenadas Polares
// Descrição: recebe as coordenadas polares de um determinado ponto no
//			  espaço cartesiano e as converte em coordenadas x e y.
// Autor: Alyppyo Coutinho
// Data: 30/04/2019

// Cabeçalho
#include <stdio.h>
#include <math.h>

// Assinatura da função
void coordenadasCartesianas(double raio, double angulo,	double * x, double * y);

// Função principal
int main() {
	// Declaração de variáveis
	double raio, angulo;
	double x, y;

	// Apresentação do programa
	printf("-- Coordenadas Polares --\n");

	// Solicitar coordenadas ao usuário
	printf("- Entre com o raio: ");
	scanf("%lf", &raio);

	printf("- Entre com o ângulo: ");
	scanf("%lf", &angulo);

	// Converter para coordenadas cartesianas
	coordenadasCartesianas(raio, angulo, &x, &y);

	// Imprimir os valores
	printf("> Coordenadas cartesianas: x = %lf, y = %lf.\n", x, y);

	// Retorno da função
	return 0;
}


// Função: coordenadasCartesianas
// Descrição: converte coordenadas polares para cartesianas.
void coordenadasCartesianas(double raio, double angulo, // Passagem por valor
							double * x, double * y)     // Passagem por referência
{
	*x = raio * cos(angulo);
	*y = raio * sin(angulo);
}








