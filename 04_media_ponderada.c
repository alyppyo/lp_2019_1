// Programa: Média ponderada
// Descrição: recebe três valores do usuário e calcula sua média ponderada.
// Autor: Alyppyo Coutinho
// Data: 14/03/2019

// Cabeçalho
#include <stdio.h>

// Função principal
int main() {
	// Declaração de variáveis
	float numero1, numero2, numero3;
	float mediaPonderada;

	// Apresentação do programa
	printf("-- Média Ponderada --\n");
	
	// Solicitar números ao usuário
	printf("- Entre com o primeiro número: ");
	scanf("%f", &numero1);

	printf("- Entre com o segundo número: ");
	scanf("%f", &numero2);

	printf("- Entre com o terceiro número: ");
	scanf("%f", &numero3);

	// Calcular a média ponderada
	mediaPonderada = (4*numero1 + 5*numero2 + 6*numero3) / 15;

	// Divulgar o valor da média
	printf("> Média ponderada: %f\n", mediaPonderada);

	// Retorno da função
	return 0;
}


