// Função: Fatorial (recursivo)
// Descrição: realiza o cálculo do fatorial de um número de forma recursiva.
// Autor: Alyppyo Coutinho
// Data: 30/04/2019

// Cabeçalho
#include <stdio.h>

// Assinatura da função
int fatorial(int numero);

// Função principal
int main() {
	// Declaração de variáveis
	int numero;

	// Apresentação do programa
	printf("-- Fatorial Recursivo --\n");

	// Solicitar um número natural ao usuário
	printf("- Entre com um número natural: ");
	scanf("%d", &numero);

	// Divulgar o fatorial
	printf("> Fatorial: %d\n, ", fatorial(numero));

	// Retorno da função
	return 0;
}


// Função: fatorial
// Descrição: retorna o fatorial de um número passado como parâmetro.
int fatorial(int numero) {
	// Caso base
	if(numero == 0) return 1;

	// Caso recursivo
	else return numero * fatorial(numero-1);
}


