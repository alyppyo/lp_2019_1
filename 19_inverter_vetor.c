// Programa: Inverte vetor
// Descrição: inverte o conteúdo de um vetor.
// Autor: Alyppyo Coutinho
// Data: 14/05/2019

// Cabeçalhos
#include <stdio.h>

// Função principal
int main() {		
	// Declaração de constantes
	const int TAM = 10;

	// Declaração de variáveis
	int vet[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int i, aux;

	// Apresentação do programa
	printf("-- Inverter Vetor --\n");

	// Imprimir o conteúdo original do vetor
	printf("> Vetor original: ");
	for(i = 0; i < TAM; i++)
		printf("%d ", vet[i]);

	// Inverter o vetor
	for(i = 0; i < TAM/2; i++) {
		aux = vet[i];
		vet[i] = vet[TAM-1-i];
		vet[TAM-1-i] = aux;
	}

	// Imprimir o conteúdo invertido do vetor
	printf("\n> Vetor invertido: ");
	for(i = 0; i < TAM; i++)
		printf("%d ", vet[i]);

	puts("");

	// Retorno da função
	return 0;
}







