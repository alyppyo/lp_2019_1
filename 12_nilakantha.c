// Programa: Cálculo de Pi via Método de Nilakantha.
// Descrição: cálculo de uma aproximação do valor da constante Pi
//			  através do método de convergência rápida de Nilakantha.
// Autor: Alyppyo Coutinho
// Data: 11/04/2019

// Cabeçalhos
#include <stdio.h>

// Função principal
int main() {
	// Declaração de variáveis
	int numTermos, cont, termoDenominador = 2, sinal = 1;
	double pi = 3.0;

	// Apresentação do programa
	printf("-- Pi via Nilakantha --\n");

	// Solicitar a quantidade de termos ao usuário
	printf("- Entre com a quantidade de termos: ");
	scanf("%d", &numTermos);

	// Calcular a aproximação de pi
	for(cont = 2; cont <= numTermos; cont++) {
		pi += sinal * 4.0/(termoDenominador *
						(termoDenominador+1) *
						(termoDenominador+2));
		sinal *= -1;
		termoDenominador += 2;
	}

	// Divulgar o resultado
	printf("\n> Pi = %.7lf\n", pi);

	// Retorno da função
	return 0;
}





