// Programa: Autenticação
// Descrição: recebe um usuário e uma senha e verifica se eles estão aptos
//			  a acessar o sistema.
// Autor: Alyppyo Coutinho
// Data: 11/06/2019

// Cabeçalho
#include <stdio.h>
#include <string.h>

// Função principal
int main() {
	// Declaração de variáveis
	FILE * arquivo = NULL;
	char usuario[20];
	char senha[30], senhaValidacao[30];

	// Apresentação do programa
	printf("-- Autenticação --\n");

	// Solicitar usuário e senha
	printf("- Entre com o usuário: ");
	scanf("%s", usuario);

	// Verificar se o usuário existe. Em nosso caso, o usuário representará
	// o nome do arquivo.
	arquivo = fopen(usuario, "r");

	if(arquivo == NULL) {
		// Se o arquivo não existe, vou criá-lo.
		printf("> Usuário não encontrado! Criando arquivo.\n");
		arquivo = fopen(usuario, "w");

		// Pedir para o usuário informar uma senha
		printf("> Informe uma senha: ");
		fgets(senha, sizeof(senha), stdin);
		fgets(senha, sizeof(senha), stdin);

		// Grava senha no arquivo
		fprintf(arquivo, "%s", senha);

		// Mensagem de aviso
		printf("> Usuário gravado com sucesso!\n");
	}
	else {
		// Ler conteúdo do arquivo
		fgets(senhaValidacao, sizeof(senhaValidacao), arquivo); 

		// Pedir para o usuário informar uma senha
		printf("> Informe uma senha: ");
		fgets(senha, sizeof(senha), stdin);
		fgets(senha, sizeof(senha), stdin);

		// Verificar se senhas são iguais.
		if(!strcmp(senha, senhaValidacao)) {
			// Dar boas vindas ao usuário
			printf("> Bem vindo ao sistema!\n");
		}
		else {
			// Senha inválida
			printf("> Senha inválida!\n");
		}
	}

	// Encerrar o arquivo
	fclose(arquivo);

	// Retorno da função
	return 0;
}




