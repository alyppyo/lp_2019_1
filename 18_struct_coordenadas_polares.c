// Programa: Coordenadas polares (com struct)
// Descrição: transforma as coordenadas polares em coordenadas cartesianas
//			  com o auxílio de registros.
// Autor: Alyppyo Coutinho
// Data: 07/05/2019

// Cabeçalho
#include <stdio.h>
#include <math.h>

// Structs

// Ponto no espaço cartesiano representado em duas coordenadas: x e y.
typedef struct stPonto {
	double x;
	double y;
} Ponto;

// Assinatura da função
void coordenadasCartesianas(double raio, double angulo,	Ponto * ponto);

// Função principal
int main() {
	// Declaração de variáveis
	double raio, angulo;
	Ponto ponto;

	// Apresentação do programa
	printf("-- Coordenadas Polares --\n");

	// Solicitar coordenadas ao usuário
	printf("- Entre com o raio: ");
	scanf("%lf", &raio);

	printf("- Entre com o ângulo: ");
	scanf("%lf", &angulo);

	// Converter para coordenadas cartesianas
	coordenadasCartesianas(raio, angulo, &ponto);

	// Imprimir os valores
	printf("> Coordenadas cartesianas: x = %lf, y = %lf.\n", ponto.x, ponto.y);

	// Retorno da função
	return 0;
}


// Função: coordenadasCartesianas
// Descrição: converte coordenadas polares para cartesianas.
void coordenadasCartesianas(double raio, double angulo, // Passagem por valor
							Ponto * ponto)     // Passagem por referência
{
	ponto->x = raio * cos(angulo);
	ponto->y = raio * sin(angulo);
}








