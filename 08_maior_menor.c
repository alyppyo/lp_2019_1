// Programa: Maior e menor número
// Descrição: recebe 10 números do usuário e indica qual o menor
//			  e qual o maior valor entre eles.
// Autor: Alyppyo Coutinho
// Data: 26/03/2019

// Cabeçalho
#include <stdio.h>

// Função principal
int main() {
	// Declaração de variáveis
	int numero; 	  // Armazena os valores fornecidos pelo usuário a cada iteração
	int maior, menor; // Indica qual o maior e qual o menor número fornecido
	int cont; // Variável de controle do laço	

	// Apresentação do programa
	printf("-- Maior e Menor Números --\n");

	// Solicitar os 10 valores e verificar o maior e o menor número
	for(cont = 1; cont <= 10; cont++) {
		// Solicitar os valores ao usuário
		printf("- Entre com o %dº número: ", cont);
		scanf("%d", &numero);

		// Verificar se é a primeira rodada. Caso seja,
		// o valor fornecido será o único. Logo, será o
		// maior e o menor.
		if(cont == 1) maior = menor = numero;
		// Caso contrário, já haverá um valor prévio para
		// comparação. Logo, realiza-se a comparação com
		// o novo valor fornecido.
		else {
			if(numero > maior) maior = numero;
			else if(numero < menor) menor = numero;
		}
	}

	// Divulgar o maior e o menor valor
	printf("\n> Maior número: %d", maior);
	printf("\n> Menor número: %d\n", menor);

	// Retorno da função
	return 0;
}