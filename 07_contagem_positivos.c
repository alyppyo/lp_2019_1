// Programa: Contagem de valores positivos
// Descrição: recebe do usuário 10 valores e indica quantos deles são
//			  positivos.
// Autor: Alyppyo Coutinho
// Data: 21/03/2019

// Cabeçalho
#include <stdio.h>

// Função principal
int main() {
	// Declaração de constantes
	const int QNT_VALORES = 10;

	// Declaração de variáveis
	int valor; // Recebe os valores fornecidos pelo usuário
	int contPositivos = 0; // Conta a quantidade de positivos
	int contRecebidos = 1; // Variável de controle do laço de captura

	// Apresentação do programa
	printf("-- Contagem de Valores Positivos --\n");

	// Solicitar os 10 valores ao usuário e verificar quantos são positivos
	while(contRecebidos <= QNT_VALORES) {
		printf("- Entre com o valor %d/%d: ", contRecebidos, QNT_VALORES);
		scanf("%d", &valor);

		// Verificar se o valor recebido é positivo
		if(valor > 0) contPositivos++;

		// Atualizar o contador de valores recebidos
		contRecebidos++;
	}

	// Divulgar a quantidade de valores	
	printf("\n> Quantidade de positivos: %d.\n", contPositivos);

	// Retorno da função
	return 0;
}
